# revanced-yt-builder

This is bash script to patch youtube using ![ReVanced CLI](https://github.com/revanced/revanced-cli)

---

# Dependencies
- `wget`
- Zulu's OpenJDK 17
- Keystore file
- bcprov
    - Debian: `libbcprov-java` 
    - Arch Linux: `bcprov`
---

# Downloading script
`git clone --depth 1 https://codeberg.org/Luciogi/revanced-yt-builder`

---

# Setup
`cd revanced-yt-builder`
## Generating Keystore file
```
mkdir key && cd key
```
 - `-providerpath` path may very with distro
    - Debian:/usr/share/java/bcprov.jar
    - Arch:/usr/share/java/bcprov/bcprov-jdk18on-172.jar

```
keytool -genkey -v -keystore patched-yt.keystore -alias patchedyt -provider org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath "/usr/share/java/bcprov/bcprov-jdk18on-172.jar" -storetype BKS -keyalg RSA -keysize 2048 -validity 10000
```
 - You will be asked for password create, Enter that password in `keyPass.txt`.

## Customization
- For Normal users, they should edit between lines these lines (https://codeberg.org/Luciogi/revanced-yt-builder/src/commit/deeb9cb86be4227de124b00df5c5edfaa7b08c79/build.sh#L3) and (https://codeberg.org/Luciogi/revanced-yt-builder/src/commit/deeb9cb86be4227de124b00df5c5edfaa7b08c79/build.sh#L46)
    - You need to change 
    ```
    patchVer=2.170.0            # patches version
    integrationVer=0.104.0      # integration version
    cliVer=2.20.1               # revanced-cli verion
    ytVer=18.15.40              # This is youtube apk version
    ```
    - For first time you **MUST** setup **KeyStoreFile** and **keyPassFile** variables, otherwise build will fails
    ```
    keyStoreFile=key/keyStore.keystore  # Add here your own keystore path
    keyPassFile=key/keyPass.txt     # Store key's password in a file and put path here
    ```
    - Adding/Removing youtube patch
        - Check patches name ![here](https://github.com/revanced/revanced-patches#-comgoogleandroidyoutube)
        - **Note**: Last line, e.g https://codeberg.org/Luciogi/revanced-yt-builder/src/commit/deeb9cb86be4227de124b00df5c5edfaa7b08c79/build.sh#L44 **MUST NOT** CONTAIN `\` on end
    - You may need to add Zulu OpenJDK path https://codeberg.org/Luciogi/revanced-yt-builder/src/commit/deeb9cb86be4227de124b00df5c5edfaa7b08c79/build.sh#L50

## Adding stock youtube apk
- I like to download apk from ApkMirror website
- Rename apk to `youtube-VERSION.apk` e.g youtube-18.15.40. Note: This version must match in script as `ytVer`
- Put this apk in `in/`

## Start build
## Patched apk will be placed in `out/`
---

# Usage
Before using make script executable by following command
`chmod +x build.sh`
- Print help menu
    ```
    ./build.sh
    ```
- Start patching process

    ```
    ./build.sh start
    ```
- Cleaning build files
    ```
    ./build.sh cleanall
    ```