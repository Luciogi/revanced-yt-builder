#!/bin/bash

##### Customization_Start: Edit Below Variables
patchVer=2.180.0
integrationVer=0.111.1
cliVer=2.21.5
ytVer=18.19.35
keyStoreFile=key/keyStore.keystore # Add here your own keystore path
keyPassFile=key/keyPass.txt # Store key's password in a file and put path here

# Include patches here
runRevanced(){
java -jar revanced-cli-$cliVer-all.jar \
	--keystore "$keyStoreFile"  \
		-p "$(cat $keyPassFile)" \
	-a in/youtube-$ytVer.apk \
	-o out/patched-$ytVer.apk \
	-b patches/patches-$patchVer.jar \
	-m integrations/integrations-$integrationVer.apk \
	-c \
	-t tmp \
	--exclusive \
	-i background-play \
	-i video-ads \
	-i vanced-microg-support \
	-i theme \
	-i swipe-controls \
	-i spoof-signature-verification \
	-i seekbar-tapping \
	-i return-youtube-dislike \
	-i premium-heading \
	-i old-quality-layout \
	-i minimized-playback \
	-i hide-watermark \
	-i hide-shorts-button \
	-i hide-floating-microphone-button \
	-i hide-create-button \
	-i hide-breaking-news-shelf \
	-i hide-ads \
	-i downloads \
	-i disable-auto-captions \
	-i custom-branding \
	-i client-spoof \
	-i enable-debugging \
	-i hide-endscreen-cards
}
##### Customization_End



export PATH="/usr/lib/jvm/zulu-17/bin/:$PATH"

checkKey(){
	if [ -e "$keyPassFile" ] && [ -e "$keyStoreFile" ]; then
		return 0
	else
		return 1
	fi
}

alert(){
	message1="ERROR: Please Generate keystore, save its password in file and modify the script"
	echo -e "\e[31m$message1\e[0m" # Red colored
}

cleanall(){
	rm -rf patches integrations out tmp revanced-cli*.jar
}

createDir(){
mkdir patches integrations out in tmp key
}

getFiles(){
wget  -nc https://github.com/revanced/revanced-patches/releases/download/v$patchVer/revanced-patches-$patchVer.jar --output-document=./patches/patches-$patchVer.jar
wget  -nc https://github.com/revanced/revanced-integrations/releases/download/v$integrationVer/revanced-integrations-$integrationVer.apk --output-document=./integrations/integrations-$integrationVer.apk
wget  -nc https://github.com/revanced/revanced-cli/releases/download/v$cliVer/revanced-cli-$cliVer-all.jar
}

case $1 in

"start")

	if checkKey ; then
				createDir
				getFiles
				runRevanced
	else
		alert
	fi
	;;

"cleanall")
		cleanall
	;;

*)
		echo "Possible commands are:"
		echo "	$ ./build.sh start		# This command compiles apk"
		echo "	$ ./build.sh cleanall		# This command deletes all generated files (except key/ and options.toml)"
		echo "--------------------------------------------------------------------------------"
		echo "key/				It stores keystore and keypassword"
		echo "tmp/				It store temporary files during compiling"
		echo "in/				It store apk file you want to patch e.g youtube-18.15.40.apk)"
		echo "integrations/ 			It stores integrations file"
		echo "patches/			It stores patches file"
		echo "out/				It stores patched apk"
	;;

esac
